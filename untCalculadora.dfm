object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 201
  ClientWidth = 358
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object edtVisor: TEdit
    Left = 48
    Top = 16
    Width = 278
    Height = 21
    TabOrder = 0
  end
  object btn7: TButton
    Left = 48
    Top = 56
    Width = 65
    Height = 25
    Caption = '7'
    TabOrder = 1
    OnClick = btn7Click
  end
  object btn8: TButton
    Left = 119
    Top = 56
    Width = 65
    Height = 25
    Caption = '8'
    TabOrder = 2
    OnClick = btn8Click
  end
  object btn9: TButton
    Left = 190
    Top = 56
    Width = 65
    Height = 25
    Caption = '9'
    TabOrder = 3
    OnClick = btn9Click
  end
  object btnSoma: TButton
    Left = 261
    Top = 56
    Width = 65
    Height = 25
    Caption = '+'
    TabOrder = 4
    OnClick = btnSomaClick
  end
  object btn4: TButton
    Left = 48
    Top = 87
    Width = 65
    Height = 25
    Caption = '4'
    TabOrder = 5
    OnClick = btn4Click
  end
  object btn5: TButton
    Left = 119
    Top = 87
    Width = 65
    Height = 25
    Caption = '5'
    TabOrder = 6
    OnClick = btn5Click
  end
  object btn6: TButton
    Left = 190
    Top = 87
    Width = 65
    Height = 25
    Caption = '6'
    TabOrder = 7
    OnClick = btn6Click
  end
  object btnSubtrai: TButton
    Left = 261
    Top = 87
    Width = 65
    Height = 25
    Caption = '-'
    TabOrder = 8
    OnClick = btnSubtraiClick
  end
  object btn2: TButton
    Left = 119
    Top = 118
    Width = 65
    Height = 25
    Caption = '2'
    TabOrder = 9
    OnClick = btn2Click
  end
  object btnMultiplica: TButton
    Left = 261
    Top = 118
    Width = 65
    Height = 25
    Caption = 'x'
    TabOrder = 10
    OnClick = btnMultiplicaClick
  end
  object btn3: TButton
    Left = 190
    Top = 118
    Width = 65
    Height = 25
    Caption = '3'
    TabOrder = 11
    OnClick = btn3Click
  end
  object btn1: TButton
    Left = 48
    Top = 118
    Width = 65
    Height = 25
    Caption = '1'
    TabOrder = 12
    OnClick = btn1Click
  end
  object btn0: TButton
    Left = 119
    Top = 149
    Width = 65
    Height = 25
    Caption = '0'
    TabOrder = 13
    OnClick = btn0Click
  end
  object btnDivide: TButton
    Left = 261
    Top = 149
    Width = 65
    Height = 25
    Caption = '/'
    TabOrder = 14
    OnClick = btnDivideClick
  end
  object btnIgual: TButton
    Left = 190
    Top = 149
    Width = 65
    Height = 25
    Caption = '='
    TabOrder = 15
    OnClick = btnIgualClick
  end
  object btnLimpar: TButton
    Left = 48
    Top = 149
    Width = 65
    Height = 25
    Caption = 'C'
    TabOrder = 16
    OnClick = btnLimparClick
  end
end
