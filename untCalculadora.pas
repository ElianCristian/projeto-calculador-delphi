unit untCalculadora;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    edtVisor: TEdit;
    btn7: TButton;
    btn8: TButton;
    btn9: TButton;
    btnSoma: TButton;
    btn4: TButton;
    btn5: TButton;
    btn6: TButton;
    btnSubtrai: TButton;
    btn2: TButton;
    btnMultiplica: TButton;
    btn3: TButton;
    btn1: TButton;
    btn0: TButton;
    btnDivide: TButton;
    btnIgual: TButton;
    btnLimpar: TButton;
    procedure btn0Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure btn6Click(Sender: TObject);
    procedure btn7Click(Sender: TObject);
    procedure btn8Click(Sender: TObject);
    procedure btn9Click(Sender: TObject);
    procedure btnSomaClick(Sender: TObject);
    procedure btnSubtraiClick(Sender: TObject);
    procedure btnMultiplicaClick(Sender: TObject);
    procedure btnDivideClick(Sender: TObject);
    procedure btnIgualClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  valor1 : Real;
  valor2 : Real;
  funcao : Integer;

implementation

{$R *.dfm}

procedure TForm1.btn0Click(Sender: TObject);
begin
  edtVisor.Text := edtVisor.Text + (Sender as TButton).Caption
end;

procedure TForm1.btn1Click(Sender: TObject);
begin
  edtVisor.Text := edtVisor.Text + (Sender as TButton).Caption
end;

procedure TForm1.btn2Click(Sender: TObject);
begin
  edtVisor.Text := edtVisor.Text + (Sender as TButton).Caption
end;

procedure TForm1.btn3Click(Sender: TObject);
begin
  edtVisor.Text := edtVisor.Text + (Sender as TButton).Caption
end;

procedure TForm1.btn4Click(Sender: TObject);
begin
edtVisor.Text := edtVisor.Text + (Sender as TButton).Caption
end;

procedure TForm1.btn5Click(Sender: TObject);
begin
  edtVisor.Text := edtVisor.Text + (Sender as TButton).Caption
end;

procedure TForm1.btn6Click(Sender: TObject);
begin
  edtVisor.Text := edtVisor.Text + (Sender as TButton).Caption
end;

procedure TForm1.btn7Click(Sender: TObject);
begin
  edtVisor.Text := edtVisor.Text + (Sender as TButton).Caption
end;

procedure TForm1.btn8Click(Sender: TObject);
begin
  edtVisor.Text := edtVisor.Text + (Sender as TButton).Caption
end;

procedure TForm1.btn9Click(Sender: TObject);
begin
  edtVisor.Text := edtVisor.Text + (Sender as TButton).Caption
end;

procedure TForm1.btnDivideClick(Sender: TObject);
begin
  valor1 := StrToFloat(edtVisor.Text);
  edtVisor.Text := '';
  funcao := 3;
end;

procedure TForm1.btnIgualClick(Sender: TObject);
var
  soma : real;
  somaInt : Integer;
begin
  valor2:=StrToInt(edtVisor.Text);
  case (funcao) of

  1:
  begin
    soma:=valor1+valor2;
    edtVisor.text:=FloatToStr(soma);
  end;
  2:
  begin
    soma:=valor1-valor2;
    edtVisor.text:=FloatToStr(soma);
  end;
  4:
  begin
    soma:=valor1*valor2;
    edtVisor.text:=FloatToStr(soma);
  end;
  3:
  begin
    if(valor2<>0)then
      begin
        soma:=valor1/valor2;
        edtVisor.text:=FloatToStr(soma);
      end
    else
      begin
        SHowMessage('Divis�o por zero!!');
        edtVisor.Text:='ERRO';
      end
    end
end;  //finaliza o  case
end;

procedure TForm1.btnLimparClick(Sender: TObject);
begin
   edtVisor.Text:='';
end;

procedure TForm1.btnMultiplicaClick(Sender: TObject);
begin
  valor1 := StrToFloat(edtVisor.Text);
  edtVisor.Text := '';
  funcao := 4;
end;

procedure TForm1.btnSomaClick(Sender: TObject);
begin
  valor1 := StrToFloat(edtVisor.Text);
  edtVisor.Text := '';
  funcao := 1;
end;

procedure TForm1.btnSubtraiClick(Sender: TObject);
begin
  valor1 := StrToFloat(edtVisor.Text);
  edtVisor.Text := '';
  funcao := 2;
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_NUMPAD1 then
    btn1.Click;
  if Key = VK_NUMPAD2 then
    btn2.click;
  if Key = VK_NUMPAD3 then
    btn3.click;
  if Key = VK_NUMPAD4 then
    btn4.click;
  if Key = VK_NUMPAD5 then
    btn5.click;
  if Key = VK_NUMPAD6 then
    btn6.click;
  if Key = VK_NUMPAD7 then
    btn7.click;
  if Key = VK_NUMPAD8 then
    btn8.click;
  if Key = VK_NUMPAD9 then
    btn9.click;
  if Key = VK_NUMPAD0 then
    btn0.click;
  if Key = VK_ADD then
    btnSoma.click;
  if Key = VK_SUBTRACT then
    btnSubtrai.click;
  if Key = VK_MULTIPLY then
    btnMultiplica.click;
  if Key = VK_DIVIDE then
    btnDivide.click;
  if Key = VK_RETURN then
    btnIgual.click;

  if Key = VK_DELETE then
    btnLimpar.click;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  Valor1 := 0;
  valor2 := 0;
end;

end.
